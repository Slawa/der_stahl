/* global $ */

import jQueryPlugins from "./../plugins/common.js";
import ScrollToEl from "./../widgets/scrollToEl.js";
import Menu from "./../widgets/menu.js";
import Form from "./../widgets/form.js";
import Tab from "./../widgets/tab.js";
import Zoom from "./../widgets/zoom.js";
import Diffrence from "./../widgets/difference.js";

$(function(){
    jQueryPlugins.sliderMain('.js-slider-front');
    jQueryPlugins.feedbackSlider('.js-feedback-slider');
    jQueryPlugins.placeholder('.js-placeholder');
    jQueryPlugins.phone('a[href^="tel:"]');
    jQueryPlugins.fancybox('.js-fancybox');

    ScrollToEl.plugin('[data-target]');
    Menu.plugin('.js-menu');
    Form.plugin('.js-form');
    Tab.plugin('.js-tab');
    Zoom.plugin('.js-zoom');
    Diffrence.plugin('.js-diffrence', {
        'money' :   [90, 40, 90, 60, 30],
        'date' :    [100, 50, 70, 50, 20],
        'energy' :  [70, 40, 90, 60, 30],
        'speed' :   [40, 40, 70, 60, 30],
        'fire' :    [50, 40, 60, 70, 30],
        'convenience' : [50, 20, 30, 60, 30],
        'ecology' : [50, 40, 90, 60, 30]
    });

    $('#preloader').delay(1000).fadeOut();
});