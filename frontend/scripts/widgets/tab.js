export default class Widget {

    constructor(element) {
        this.$root = $(element);
        this.locals = this._getDom();
        this.scrolls = [];

        this._initHover();
        this._setScrollWith();
        this._initScrolls();

        this._assignEvents();
    }

    _getDom(){
        const $root = this.$root;

        return {
            $hover: $root.find('[data-nav-active]'),
            $navItems: $root.find('[data-tab-item]'),
            $contentItems: $root.find('[data-tab-content]'),
            $scrollInner: $root.find('[data-scroll-width]'),
            $scrolls: $root.find('[ data-tab-scroll]')
        }
    }

    _initHover(){
        const $firstLink = this.locals.$navItems.eq(0);

        this.locals.$hover
            .css({
                left: $firstLink.position().left,
                top: $firstLink.position().top,
                width: $firstLink.outerWidth()
            })
            .fadeIn(200)
            .addClass('show-active');
    }

    _setScrollWith(){
        this.locals.$scrollInner.each( (index, el) => {
            const $el = $(el);
            const width = $el.children().length * 500;

            $el.css('width', width);
        })
    }

    _initScrolls(){
        this.locals.$scrolls.each( (index, el) => {
            const scroll = $(el).jScrollPane({
                horizontalDragMinWidth: 124,
                horizontalDragMaxWidth: 124,
                horizontalTrackWidth: 524,
                verticalGutter: 40,
                horizontalGutter: 40
            }).data('jsp');

            this.scrolls.push(scroll);
        })
    }

    _assignEvents() {
        const self = this;

        this.$root
            .on('mouseenter', '[data-tab-item]', this._onEnterNav.bind(this))
            .on('mouseleave', '[data-tab-nav]', this._onLeaveNav.bind(this))
            .on('click', '[data-tab-item]', this._onClickNav.bind(this));

        $(window).resize( () => {
            self._updateScroll();
        })
    }

    _onEnterNav(e){
        const $link = $(e.currentTarget);

        this.locals.$hover.animate({
            left: $link.position().left,
            top: $link.position().top,
            width: $link.outerWidth()
        }, 200);
    }

    _onLeaveNav(e){
        const $activeLink = this.locals.$navItems.filter('.active').first();

        this.locals.$hover.animate({
            left: $activeLink.position().left,
            top: $activeLink.position().top,
            width: $activeLink.outerWidth()
        }, 200);
    }

    _onClickNav(e){
        e.preventDefault();
        const $link = $(e.currentTarget);
        const locals = this.locals;
        const $content = $($link.attr('href'));

        if ($link.hasClass('active') || !$content.length) return;

        locals.$navItems.removeClass('active');
        locals.$contentItems.removeClass('active');

        $link.addClass('active');
        setTimeout( () => {
            $content.addClass('active');
            this._updateScroll();
        }, 400);
    }

    _updateScroll(){
        this.scrolls.forEach( (item) => {
            item.reinitialise();
        })
    }

    // static
    static plugin(selector) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.tab');

            if (!data) {
                data = new Widget(el);
                $element.data('widget.tab', data);
            }
        })
    }
}


