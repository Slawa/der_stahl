export default class Widget {

    constructor(element) {
        this.$root = $(element);
        this.menuTimer = null;
        this.$hover = this.$root.find('[data-nav-active]');

        this._assignEvents();
    }

    _assignEvents() {
        this.$root
            .on('mouseenter', '[data-nav-item]', this._onEventEnter.bind(this))
            .on('mouseleave', '[data-nav-item]', this._onEventLeave.bind(this));
    }

    _onEventEnter(e){
        const $currentLink = $(e.currentTarget);
        const $hover = this.$hover;
        const curPosition = $currentLink.position().left;

        clearTimeout(this.menuTimer);

        if (!$hover.hasClass('show-active')) {
            $hover
                .css({
                    left: curPosition,
                    width: $currentLink.outerWidth()
                })
                .addClass('show-active')
                .fadeIn(200);
        } else {
            $hover
                .stop(true, true)
                .animate({
                    left: curPosition,
                    width: $currentLink.outerWidth()
                }, 200);
        }
    }

    _onEventLeave(){
        this.menuTimer = setTimeout( () =>{
            this.$hover
                .removeClass('show-active')
                .fadeOut(200);
        }, 2000);
    }

    // static
    static plugin(selector) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.menu');

            if (!data) {
                data = new Widget(el);
                $element.data('widget.menu', data);
            }
        })
    }
}


