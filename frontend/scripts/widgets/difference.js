export default class Widget {

    constructor(element, data) {
        this.$root = $(element);
        this.locals = this._getDom();
        this.timer = null;
        this.currentIndex = null;
        this.data = data;
        this.isPausing = false;

        this._init();
        this._assignEvents();

        this.slideTo(0);
    }

    _getDom(){
        const $root = this.$root;

        return {
            $hover: $root.find('[data-diff-active]'),
            $navItems: $root.find('[data-diff-nav]'),
            $rowValues: $root.find('[data-diff-value]'),
            $rowProgresses: $root.find('[data-diff-progress]')
        }
    }

    _init(){
        const $firstLink = this.locals.$navItems.eq(0);

        this.locals.$hover
            .css({
                left: $firstLink.eq(0).position().left,
                top: $firstLink.eq(0).position().top,
                width: $firstLink.eq(0).outerWidth()
            })
            .fadeIn(200)
            .addClass('show-active');
    }

    _assignEvents() {
        this.$root
            .on('mouseenter', '[data-diff-nav]', this._onEnterLink.bind(this))
            .on('mouseleave', '[data-diff-con]', this._onLeaveNavigation.bind(this))
            .on('click', '[data-diff-nav]', this._onClickNav.bind(this));
    }

    _onEnterLink(e){
        const $link = $(e.currentTarget);

        this.locals.$hover.animate({
            left: $link.position().left,
            top: $link.position().top,
            width: $link.outerWidth()
        }, 200);
        this.pause();
    }

    _onLeaveNavigation(){
        const $activeLink = this.locals.$navItems.filter('.active').first();

        this.locals.$hover.animate({
            left: $activeLink.position().left,
            top: $activeLink.position().top,
            width: $activeLink.outerWidth()
        }, 200);
        this.resume();
    }

    _onClickNav(e){
        e.preventDefault();
        const $link = $(e.currentTarget);
        const newIndex = this.locals.$navItems.index($link);

        this._showSlide(newIndex);
    }

    _getNextIndex(){
        const length = this.locals.$navItems.length -1;
        return (length === this.currentIndex)? 0: this.currentIndex + 1;
    }

    slideTo(index){
        this._showSlide(index);
        this.run();
    }

    pause(){
        this.isPausing = true;
        clearTimeout(this.timer);
    }

    resume(){
        if (!this.isPausing) return;
        this.isPausing = false;
        this.run();
    }

    run(){
        clearTimeout(this.timer);
        this.timer = setTimeout( () => {
            const newIndex = this._getNextIndex();
            this._showSlide(newIndex);
        }, 5000);
    }

    _showSlide(index){
        const locals = this.locals;
        const $curLink = locals.$navItems.eq(index);

        if (this.currentIndex == index) return;

        this.currentIndex = index;
        console.log(index);

        locals.$navItems.removeClass('active');
        locals.$hover.animate({
            left: $curLink.position().left,
            top: $curLink.position().top,
            width: $curLink.outerWidth()
        },400);
        $curLink.addClass('active');
        
        this._updateViewData();
    }

    _getData(){
        const $activeLink= this.locals.$navItems.eq(this.currentIndex);
        return this.data[$activeLink.data('diff-info')];
    }

    _updateViewData(){
        const data = this._getData() || null;
        const locals = this.locals;

        if (!data) return;
        for (let i = 0, n = data.length; i < n; i++) {
            let currentValue = data[i] || 0;

            locals.$rowProgresses.eq(i).css('width', currentValue * 1.8);
            locals.$rowValues.eq(i).text(currentValue);
        }
    }

    // static
    static plugin(selector, config) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.difference');

            if (!data) {
                data = new Widget(el, config);
                $element.data('widget.difference', data);
            }
        })
    }
}


