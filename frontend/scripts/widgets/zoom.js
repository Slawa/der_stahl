export default class Widget {

    constructor(element) {
        this.$root = $(element);
        this.isActive = null;
        this.locals = this._getDom();

        this._initZoom();
        this._assignEvents();
        this.showByIndex(0);
    }

    _getDom(){
        const $root = this.$root;

        return {
            $zoomy: $root.find('[data-zoom]'),
            $slides: $root.find('[data-module-item]'),
            $pins: $root.find('[data-module-draft]'),
            $navs: $root.find('[data-module-link]')
        }
    }

    _initZoom(){
        const $zoom = this.locals.$zoomy;

        $zoom
            .data({
                'left': $zoom.offset().left,
                'top': $zoom.offset().top
            })
            .zoomy();
    }

    _assignEvents() {
        this.$root
            .on('mouseenter', '[data-zoom-img]', this.activatePlugin.bind(this))
            .on('click', '[data-module-link]', this._onClickNavigation.bind(this))

        this.locals.$zoomy
            .on('mousemove', this._onImageMove.bind(this))
    }

    activatePlugin(e){
        const $img = $(e.currentTarget);

        if (this.isActive) return;
        $img.trigger('click');
    }

    _onClickNavigation(e){
        e.preventDefault();
        const currentIndex = $(e.currentTarget).index();

        this.showByIndex(currentIndex);
    }

    /**
     * Show slide by index
     * @param {Number} index
     * @private
     */
    showByIndex(index){
        const locals = this.locals;

        if (locals.$navs.eq(index).hasClass('active')) return;

        locals.$slides
            .removeClass('active')
            .stop(true, true).fadeOut();
        locals.$navs.removeClass('active');
        locals.$pins.removeClass('show-image');

        locals.$navs.eq(index).addClass('active');
        setTimeout( () => {
            locals.$pins.eq(index).addClass('show-image');
            locals.$slides.eq(index).addClass('active').fadeIn();
        }, 500)
    }

    _onImageMove(e){
        const $zoomImg = this.locals.$zoomy;
        const leftPosition = e.pageX - $zoomImg.data('left');
        const topPosition = e.pageY - $zoomImg.data('top');
        let wi = 50;
        let activeSlide = null;

        if ((leftPosition > 180 && topPosition > 30) && (leftPosition < (180 + wi)) && (topPosition  < (30 + wi))){
            activeSlide = 0;
        }

        if ((leftPosition > 0 && topPosition > 586) && (leftPosition  < wi) && (topPosition  < (586 + wi))){
            activeSlide = 1;
        }

        if ((leftPosition > 174 && topPosition > 394) && (leftPosition  < (174 + wi)) && (topPosition < (394 + wi))){
            activeSlide = 2;
        }

        if (activeSlide){
            this.showByIndex(activeSlide);
        }
    }

    // static
    static plugin(selector) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.zoom');

            if (!data) {
                data = new Widget(el);
                $element.data('widget.zoom', data);
            }
        })
    }
}


