
export default class Widget {

    constructor(element) {
        this.$root = $(element);
        this._assignEvents();
    }

    _assignEvents() {
        this.$root
            .on('focus', '[data-placehold]', this._onElementFocus.bind(this))
            .on('blur', '[data-placehold]', this._onElementBlur.bind(this));
    }

    _onElementFocus(e){
        $(e.currentTarget).closest('[data-form-item]').addClass('active');
    }

    _onElementBlur(e){
        $(e.currentTarget).closest('[data-form-item]').removeClass('active');
    }


    // static
    static plugin(selector) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.form');

            if (!data) {
                data = new Widget(el);
                $element.data('widget.form', data);
            }
        })
    }
}














$('[data-placehold]')
    .focus(function () {
        $(this).closest('.form-item').addClass('active');
    })
    .blur( function(){
        $(this).closest('.form-item').removeClass('active');
    });