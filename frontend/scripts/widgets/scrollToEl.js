export default class Widget {

    constructor(element) {
        this.$root = $(element);
        this._assignEvents();
    }

    _assignEvents() {
        $('body')
            .on('click', '[data-target]', this._onClickTarget.bind(this));
    }

    _onClickTarget(e){
        e.preventDefault();
        const $link = $(e.currentTarget);
        const $target = $(`#${$link.data('target')}`);

        if (!$target.length) return;

        $("html, body").animate(
            {
                scrollTop: $target.offset().top
            },
            '500', 'swing');
    }

    // static
    static plugin(selector) {
        const $elems = $(selector);
        if (!$elems.length) return;

        return $elems.each(function (index, el) {
            let $element = $(el);
            let data     = $element.data('widget.scroll');

            if (!data) {
                data = new Widget(el);
                $element.data('widget.scroll', data);
            }
        })
    }
}

