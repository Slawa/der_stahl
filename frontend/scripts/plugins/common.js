let plugins = {
    sliderMain(selector){
        $(selector).royalSlider({
            arrowsNav: false,
            loop: true,
            keyboardNavEnabled: true,
            imageAlignCenter: true,
            controlsInside: false,
            imageScaleMode: 'fill',
            autoScaleSlider: true,
            autoScaleSliderHeight: 300,
            autoScaleSliderWidth: 940,
            controlNavigation: 'bullets',
            thumbsFitInViewport: false,
            navigateByClick: false,
            startSlideId: 0,
            transitionType:'fade',
            globalCaption: false,
            sliderDrag: false,
            sliderTouch: false,
            allowCSS3: false,
            deeplinking: {
                enabled: true,
                change: false
            },
            autoPlay: {
                enabled: true,
                pauseOnHover: false,
                delay: 3000
            }
        });
    },

    feedbackSlider(selector){
        $(selector).royalSlider({
            arrowsNav: false,
            loop: true,
            keyboardNavEnabled: true,
            controlNavigation: 'bullets',
            navigateByClick: false,
            startSlideId: 0,
            transitionType:'slide',
            globalCaption: false,
            sliderDrag: true,
            sliderTouch: false,
            allowCSS3: false,
            autoPlay: {
                enabled: true,
                pauseOnHover: true,
                delay: 3000
            }
        })
    },

    placeholder(selector){
        $(selector).placeholder();
    },

    phone(selector){
        const deviceAgent = navigator.userAgent.toLowerCase();
        const agentID = deviceAgent.match(/(iphone|ipod|ipad)/);

        if (agentID)  return;

        $('body').on('click', 'selector',  (e) => {
            const $link = $(e.currentTarget);
            const newUrl = $link.attr('href').replace(/^tel:/, 'callto:');

            $link.attr('href', newUrl);
        });
    },

    fancybox(selector){
        $(selector).fancybox({
            openEffect	: 'fade',
            closeEffect	: 'fade'
        });
    },

    scrollpane(selector){
        return $(selector).jScrollPane({
            horizontalDragMinWidth: 124,
            horizontalDragMaxWidth: 124,
            horizontalTrackWidth: 524,
            verticalGutter: 40,
            horizontalGutter: 40
        }).data('jsp');
    }

};

export default plugins;