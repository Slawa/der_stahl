
import mousewheel from 'jquery-mousewheel';
import jscrollpane from 'jscrollpane';
import placeholder from 'jquery-placeholder';

import fancy from './../plugins/jquery.fancybox.pack.js';
import sliderS from './../plugins/jquery.royalslider.custom.min.js';
import img from './../plugins/imgLiquid-min.js';
import zoom from './../plugins/zoomy1.0.js';

import main from './../main.js';
import slider from './../detail-slider.js';
import moduleSlider from './../module-slider.js';

$(function(){
    $('#preloader').delay(1000).fadeOut();
});